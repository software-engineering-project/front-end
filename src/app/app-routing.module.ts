import { DataEntryComponent } from './pages/data-entry/data-entry.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { ArticlesComponent } from './pages/articles/articles.component';
import { GraphicsPageComponent } from './pages/graphics-page/graphics-page.component';



const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'data-entry', component: DataEntryComponent },
  { path: "articles", component: ArticlesComponent },
  { path: "graphics", component: GraphicsPageComponent },

  // Otherwise, redirect to home
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const appRoutingModule = RouterModule.forRoot(routes);
