
  //////////////////// Modules imported \\\\\\\\\\\\\\\\\\\\

import { MatGridListModule } from '@angular/material/grid-list';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StoreModule } from '@ngrx/store';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NbThemeModule, NbLayoutModule } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { MatButtonModule } from '@angular/material/button';
import { MatTabsModule } from '@angular/material/tabs';
import { MatSelectModule } from '@angular/material/select';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import {MatIconModule} from '@angular/material/icon';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { GraphicsPageModule } from './pages/graphics-page/graphics-page.module'
import { MatTableModule } from '@angular/material/table';
import { MatDialogModule } from '@angular/material/dialog';
import { HttpClientModule } from '@angular/common/http';


  //////////////////// Components declared \\\\\\\\\\\\\\\\\\\\

import { HomeComponent } from './pages/home/home.component';
import { ArticlesComponent} from './pages/articles/articles.component';
import { DataEntryComponent } from './pages/data-entry/data-entry.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { OverlayContainer } from '@angular/cdk/overlay';




@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    DataEntryComponent,
    HomeComponent,
    ArticlesComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    StoreModule.forRoot({}, {}),
    NbThemeModule.forRoot({ name: 'default' }),
    NbLayoutModule,
    NbEvaIconsModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatGridListModule,
    MatFormFieldModule,
    MatInputModule,
    MatTabsModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatDatepickerModule,
    MatNativeDateModule,
    GraphicsPageModule,
    MatIconModule,
    MatTableModule,
    MatDialogModule,
    HttpClientModule,
  ],
  providers: [
    { provide: OverlayContainer },
  ],
  bootstrap: [AppComponent]
})
export class AppModule extends OverlayContainer{ }
