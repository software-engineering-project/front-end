import { Component, Inject } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { HttpClient } from "@angular/common/http";
import { environment } from '../../../environments/environment';
// import { Observable } from 'rxjs';

/**
 *To create an Article instance
 */
export interface Article {
  date: String;
  title: String;
  type: String;
  department: String;
  theme: String;
  link?: String;
  associated: Implication[];
}


/**
 * To create an Implication instance
 */
export interface Implication {
  _id: String;
  date: String;
  title: String;
  type: String;
  department: String;
  paper: String;
  theme: String;
  tonality: String;
  color?: String;
  link?: String;
}


@Component({
  selector: 'app-article',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.css']
})

/**
 * Page that displays articles and enables search mode
 */
export class ArticlesComponent {

    //////////////////// Data \\\\\\\\\\\\\\\\\\\\

  public listArticles : Article[] = [];
  dataSourceArticles = new MatTableDataSource(this.listArticles);
  public displayedColArticles: string[] = ["date", "title", "type", "department", "theme", "link"];
  titleArticles = "Communiqués de presse";
  obsArticles: any;


  public listImplications: Implication[] = [];
  public displayedColImplications: string[] = ["date", "title", "type", "department", "paper", "theme", "tonality", "link"];
  dataSourceImplications = new MatTableDataSource(this.listImplications);
  titleImplications = "Retombées de presse";
  obsImplications: any;

    //////////////////// Tables filter \\\\\\\\\\\\\\\\\\\\

  displayTable: String = '';
  allTables = true;

  readonly ROOT_URL = environment.API_URL ?? "http://localhost:3000";


  constructor(private http: HttpClient) {}

  ngOnInit() {
    //Get articles and implications from db and fill in the lists
    this.getArticles();
    this.getImplications();
  }


  /**
   * Get articles from db
   */
  private async getArticles() {

      //////////////////// Get request, get all the articles and their informations. \\\\\\\\\\\\\\\\\\\\

    for (let i = 0; i < 100; i++) {
      const article  = {
        date: "dateA"+i,
        title: "titleA"+i,
        type: "typeA"+i,
        department: "departmentA"+i,
        theme: "themeA"+i,
        link: "linkA"+i,
        associated: [this.listImplications[0]]  //example (linked by id)
        //articles are associated with implications with foreign keys
      }
      //Get observable
      // this.obsArticles = this.http.get(this.ROOT_URL + "/press_release");

      // for (let article of this.obsArticles) {
      //   const art = {
      //     date: article.date,
      //     title: article.title,
      //     type: article.type,
      //     department: article.department,
      //     theme: article.theme,
      //     link: article.link ?? '',
      //     associated: [this.listImplications[0]]  //example (linked by id)
      //     //articles are associated with implications with foreign keys
      //   }
      //   this.listArticles.push(art);
      // }

      this.listArticles.push(article);
    }

    /* TODO:
      sort by date  the list
    */
  }


  /**
   * Get implications (retombées) from db
   */
  private async getImplications() {
    //Get request, get all the articles and their informations.
    for (let i = 0; i < 100; i++) {
      if (i%4 != 0) {
        const implication = {
          _id: i.toString(),
          date: "dateI"+i,
          title: "titleI"+i,
          type: "typeI"+i,
          department: "departmentI"+i,
          paper: "paperI"+i,
          theme: "themeI"+i,
          tonality: "positive",
          color: "color",
          link: "linkA"+i
        }
        const color = implication.tonality.toLowerCase() == "positive" ? "rgb(147,201,14)" : "#EF426F"
        implication.color = color;
        this.listImplications.push(implication);
      } else {
        const implication = {
          _id: i.toString(),
          date: "dateI"+i,
          title: "titleI"+i,
          type: "typeI"+i,
          department: "departmentI"+i,
          paper: "paperI"+i,
          theme: "themeI"+i,
          tonality: "negative",
          color: "color",
          link: "linkA"+i
        }
        const color = implication.tonality.toLowerCase() == "positive" ? "rgb(147,201,14)" : "#EF426F"
        implication.color = color;
        this.listImplications.push(implication);
      }


      // this.obsImplications = this.http.get(this.ROOT_URL + "/articles");

      // for (let article of this.obsImplications) {
      //   const art = {
      //     _id: article._id,
      //     date: article.date,
      //     title: article.title,
      //     type: article.type,
      //     department: article.department,
      //     paper: article.paper,
      //     theme: article.theme,
      //     tonality: article.tonality,
      //     color: article.color.toLowerCase() == "positive"? "rgb(147,201,14)" : "#EF426F",
      //     link: article.link ?? ''
      //   }
      //   this.listImplications.push(art);
      // }
    }
    /* TODO:
      sort by date  the list
    */
  }


  /**
   * Filter research
   */
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    if (filterValue) {
      this.dataSourceArticles.filter = filterValue.trim().toLowerCase();
      this.dataSourceImplications.filter = filterValue.trim().toLowerCase();
    }
  }


  /**
   * When an article is clicked on, shows solely the article with its associated implications
   */
  clickedOnArticle(article: Article) {

      //////////////////// Hide search form \\\\\\\\\\\\\\\\\\\\

    const form = <HTMLElement>document.querySelector(".form");
    if (form) {
      form.style.visibility = "hidden";
    }
    this.allTables = false; //display return button
    this.titleArticles = "Communiqué de presse";
    this.titleImplications = "Retombées liées à " + article.title;


      //////////////////// Display new informations \\\\\\\\\\\\\\\\\\\\

    this.dataSourceArticles = new MatTableDataSource([article]);
    this.dataSourceImplications = new MatTableDataSource(article.associated);
  }


  /**
   * Display all articles back
   */
  backAllArticles() {
    const form = <HTMLElement>document.querySelector(".form");
    if (form) {
      form.style.visibility = "visible";
    }

    this.allTables = true;    //hide return button
    this.titleArticles = "Communiqués de presse";
    this.titleImplications = "Retombées de presse";

      //////////////////// Display all informations \\\\\\\\\\\\\\\\\\\\

    this.dataSourceArticles = new MatTableDataSource(this.listArticles);
    this.dataSourceImplications = new MatTableDataSource(this.listImplications);
  }
}

