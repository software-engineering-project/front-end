import {Component, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { environment } from '../../../environments/environment';




interface Journaux{
  name:string;
  dep:string
}

@Component({
  selector: 'app-data-entry',
  templateUrl: './data-entry.component.html',
  styleUrls: ['./data-entry.component.css'],
})



/**
 * Page to enter new articles
 */
export class DataEntryComponent implements OnInit {
  readonly ROOT_URL = environment.API_URL ?? "http://localhost:3000";


  myControlTypeCP = new FormControl();
  myControlTypeR = new FormControl();
  myControlDepartement = new FormControl();
  myControlThemeCP = new FormControl();
  myControlArticle = new FormControl();
  myControlRetombe = new FormControl();
  myControlThemeR = new FormControl();
  //This is for type from retombé
  //types is a array of all types we have
  types_retombe: string[] = ['PQR', 'Radio', 'TV', 'Web'];
  filteredtypesRetombe: Observable<string[]>;
  ngOnInit() {
    this.filteredtypesCP = this.myControlTypeCP.valueChanges.pipe(
      startWith(''),
      map(value =>this._filterCP(value)),
    );
    this.filteredtypesRetombe = this.myControlTypeR.valueChanges.pipe(
      startWith(''),
      map(value =>this._filtertypeRetombe(value)),
    );
    this.filteredDepartement = this.myControlDepartement.valueChanges.pipe(
      startWith(''),
      map(value =>this._filterDepartement(value)),
    );
    this.filteredThemeCP = this.myControlThemeCP.valueChanges.pipe(
      startWith(''),
      map(value =>this._filterThemeCP(value)),
    );
    this.filteredThemeR = this.myControlThemeR.valueChanges.pipe(
      startWith(''),
      map(value =>this._filterThemeR(value)),
    );
    this.filteredArticle = this.myControlArticle.valueChanges.pipe(
      startWith(''),
      map(value =>this._filterArticle(value)),
    );
    this.filteredRetombe = this.myControlRetombe.valueChanges.pipe(
      startWith(''),
      map(value =>this._filterRetombe(value)),
    );
  }

  private _filtertypeRetombe(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.types_retombe.filter(typer => typer.toLowerCase().indexOf(filterValue) === 0);
  }
  //fonction to add a types in array if it is not in
  public addOptiontypeRetombe(){
    let value =<HTMLInputElement> document.querySelector("#Type-retombe");
    if(this.types_retombe.indexOf(value.value)!==-1){
      //value is not in types

    }
    else{
      this.types_retombe.push(value.value)
    }
  }


  //this is for type from communique de presse
  types_cp: string[] = ['Mail','CP','IP','Partenariat','Action','DT','Radio'];
  filteredtypesCP: Observable<string[]>;

  private _filterCP(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.types_cp.filter(type => type.toLowerCase().indexOf(filterValue) === 0);
  }
  //fonction to add a types in array if it is not in
  public addOptionCP(){
    let value =<HTMLInputElement> document.querySelector("#Type-cp");
    if(this.types_cp.indexOf(value.value)!==-1){
      //value is not in types

    }
    else{
      this.types_cp.push(value.value)
    }
  }

  //this is for type from communique de presse
  departement: string[] = ['64','65','40'];
  filteredDepartement: Observable<string[]>;

  private _filterDepartement(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.departement.filter(type => type.toLowerCase().indexOf(filterValue) === 0);
  }


  //this is for Theme
  themeCP: string[] = ['Partenariat','Sécurité','Transition écologique','RH','Crise climatique','Crise RH','Coupure','Linky','RSE'];
  themeR: string[] = ['Partenariat','Sécurité','Transition écologique','RH','Crise climatique','Crise RH','Coupure','Linky','RSE'];
  filteredThemeCP: Observable<string[]>;
  filteredThemeR: Observable<string[]>;

  private _filterThemeCP(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.themeCP.filter(type => type.toLowerCase().indexOf(filterValue) === 0);
  }
  //fonction to add a types in array if it is not in
  public addOptionThemeCP(){
    let value =<HTMLInputElement> document.querySelector("#cp-theme");

    if(this.themeCP.indexOf(value.value)!==-1){
      //value is not in types

    }
    else{
      this.themeCP.push(value.value)
    }
  }


  private _filterThemeR(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.themeR.filter(type => type.toLowerCase().indexOf(filterValue) === 0);
  }
  //fonction to add a types in array if it is not in
  public addOptionThemeR(){
    let value =<HTMLInputElement> document.querySelector("#r-theme");

    if(this.themeR.indexOf(value.value)!==-1){
      //value is not in types

    }
    else{
      this.themeR.push(value.value)
    }
  }



  //this is for Theme
  article: string[] = ['Nom article 1','Nom article 2','Nom article 3','Nom article 4',];
  filteredArticle: Observable<string[]>;

  private _filterArticle(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.article.filter(type => type.toLowerCase().indexOf(filterValue) === 0);
  }



  //this is for Theme
  retombe: string[] = ['Nom retombe 1','Nom retombe 2','Nom retombe 3','Nom retombe 4',];
  filteredRetombe: Observable<string[]>;

  private _filterRetombe(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.retombe.filter(type => type.toLowerCase().indexOf(filterValue) === 0);
  }

  selectedValue: string;
  journal: Journaux[]= [{name:'Atomic Radio 40', dep:'40'},{name:'Atomic Radio 64', dep:'64'},{name:'Atomic Radio 65', dep:'65'}]

  /**
   * Empty inputs
   */
  public emptyInputs() {
      location.reload();
  }


}
