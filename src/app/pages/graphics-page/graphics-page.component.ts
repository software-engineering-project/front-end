import { Component} from '@angular/core';
import {FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { HttpClient } from "@angular/common/http";
import { environment } from '../../../environments/environment';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-graphic',
  templateUrl: './graphics-page.component.html',
  styleUrls: ['./graphics-page.component.css'],
})



/**
 * Page that displays graphics (Templates and custom)
 */
export class GraphicsPageComponent  {
  readonly ROOT_URL = environment.API_URL ?? "http://localhost:3000";

  validate = false;     //True when generate button is clicked on and informations entered are accurates
  errorOcc = false;   //Doesn't display graphic if error, display an error message

  //Selected graphic
  public matSelectedValue: string = "";

  //Selected graphic type
  public matSelectedGraphic: string = '';

  //Graphic options
  graphics : string[] = [
    "Tonalité des retombées par communiqué", "Departements des retombées par communiqué", "Type de retombées", "Type de retombées avec tonalité", "Journaux des retombées", "Départements de retombées", "Tonalité", "Retombées par thème"
  ];

  //Graphic types
  graphicTypes: string[] = [
    "Diagramme en bâtons", "Courbe", "Diagramme circulaire"
  ]

  //Will get article names from server
  articleNames: string[] = [
    "Article 1", "Article 2", "Article 3", "Article 4", "Article 5"
  ];

  public matSelectedName: string = '';

  formControlName = new FormControl('', Validators.required);



  constructor(private http: HttpClient) { }





  /**
   * Called when selection changes. Routes an other component to display it according to matSelectedValue
   */
  public manageSelection() {

      //////////////////// Display Input cp compononent \\\\\\\\\\\\\\\\\\\\

    if (this.matSelectedValue === this.graphics[0] || this.matSelectedValue === this.graphics[1]) {
      let inputCP = <HTMLElement>document.querySelector(".form-cp");
      if (inputCP)
        inputCP.style.visibility =  "visible";
    }
    else {
      let inputCP = <HTMLElement>document.querySelector(".form-cp");
      if (inputCP)
        inputCP.style.visibility =  "hidden";
    }

      //////////////////// Display Generate and Cancel buttons \\\\\\\\\\\\\\\\\\\\

    let buttonGenerate = <HTMLElement>document.querySelector(".button-generate");
    let buttonCancel = <HTMLElement>document.querySelector(".button-cancel");

    if (buttonGenerate && buttonCancel && this.matSelectedValue !== '' && this.matSelectedGraphic !== ''){
      buttonGenerate.style.visibility = "visible";
      buttonCancel.style.visibility = "visible";
    }

    this.validate = false;      //Hide graphic since selection has changed

    //Hide error if a previous error has occurred
    if (this.errorOcc)
      this.errorOcc = false;
  }



  /**
  * Clear fields and hide graphic
  */
  public reset() {
    this.matSelectedGraphic = '';
    this.matSelectedValue = '';
    this.matSelectedName = '';
    this.validate = false;
    if (this.errorOcc)
      this.errorOcc = false;

      //////////////////// Hide Generate and Cancel buttons \\\\\\\\\\\\\\\\\\\\

    let buttonGenerate = <HTMLElement>document.querySelector(".button-generate");
    let buttonCancel = <HTMLElement>document.querySelector(".button-cancel");
    let inputCP = <HTMLElement>document.querySelector(".form-cp");

    if (buttonGenerate && buttonCancel && inputCP){
      buttonGenerate.style.visibility = "hidden";
      buttonCancel.style.visibility = "hidden";
      inputCP.style.visibility = "hidden";
    }
  }



  /**
   * Generate graphics
   */
  public generateGraphic() {
    //If Code CP is required, check if it's valiee
    if (this.matSelectedValue === this.graphics[0] || this.matSelectedValue === this.graphics[1]){
        //Get codeCP input value
      if (this.matSelectedName !== '') this.validate = true;
    }

    else {
      this.validate = true;
    }
    console.log(this.matSelectedName);
  }


  /**
   * Manage errors and display an error message
   */
  public error() {
    this.errorOcc = true;
  }
}
