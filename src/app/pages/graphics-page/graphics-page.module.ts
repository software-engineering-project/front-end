import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GraphicsPageComponent } from './graphics-page.component';
import { GraphicsComponent } from './graphics/graphics.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';




@NgModule({
  declarations: [
    GraphicsPageComponent,
    GraphicsComponent,
  ],
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatSelectModule,
    RouterModule,
    MatInputModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  exports: [
    GraphicsPageComponent
  ]
})
export class GraphicsPageModule { }
