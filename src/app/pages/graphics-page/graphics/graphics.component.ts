import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import * as c3 from 'c3';

@Component({
  selector: 'app-graphics',
  templateUrl: './graphics.component.html',
  styleUrls: ['./graphics.component.css']
})
export class GraphicsComponent implements OnInit {
  @Input() graphicType: string = '';                //Selected graphic type (pie, ...)
  @Input() codeCP: string = '';                     //Code CP if required
  @Input() graphic: string = '';                    //Selected graphic
  @Input() arrayGraphics: string[] = [];            //Array of possible graphics
  @Input() arrayGraphicTypes: string[] = [];        //Array of possible graphic types
  @Output() errorOcc = new EventEmitter();          //Event emitted if an error has occurred

  gType: c3.ChartType = "line";
  dataName: string[] = [];
  data: c3.Primitive[][] = [[30, 200, 100, 400, 150, 250], [50, 20, 10, 40, 15, 25]]; //These values will come from the server

  ngOnInit(): void{
    switch (this.graphicType) {
      case (this.arrayGraphicTypes[0]):
        this.gType = "bar";
        break;
      case (this.arrayGraphicTypes[1]):
        this.gType = "line";
        break;
      case(this.arrayGraphicTypes[2]):
        this.gType = "pie";
        break;
      default:
        this.gType = "line";
    }
  }

  constructor() {
  }


  ngAfterViewInit() {

      //////////////////// Initialize data required to generate the graphic \\\\\\\\\\\\\\\\\\\\

    //Will be updated with values from the server
    switch (this.graphic){
      //Tonalité par code CP
      case (this.arrayGraphics[0]):
        this.dataName.push("Tonalité positive");
        this.dataName.push("Tonalité negative");
        //And get data concerning "tonalité" and push to this.data[0]/[1]
        break;

      //Department par code CP
      case (this.arrayGraphics[1]):
        //In the future, get possible "departements" from the server and push them to this.dataName
        this.dataName.push("65");
        this.dataName.push("64");
        //...
        //And get data concerning "departement par cp" and push to this.data for each departement
        break;

      //Type de retombées
      case (this.arrayGraphics[2]):
        //In the future, get possible "type de retombées" from the server
        this.dataName.push("WEB");
        this.dataName.push("PQR");
        //..
        //And add data concerning "type de retombées" from the server
        break;

      //Type de retombées avec tonalité
      case (this.arrayGraphics[3]):
        //In the future, get possible "type de retombées" from the server
        this.dataName.push("WEB");
        this.dataName.push("PQR");
        //..
        //And add data concerning "type de retombées avec tonalité" from the server
        break;

      //Journaux des retombées
      case (this.arrayGraphics[4]):
        //In the future get possible "journaux des retombées" from the server
        this.dataName.push("Sud-Ouest Pays Basque");
        this.dataName.push("La semaine des Pyrénées");
        /* .... */
        break;

      //Departement des retombées
      case (this.arrayGraphics[5]):
        //In the future, get possible "departements" from the server and push them to this.dataName
        this.dataName.push("65");
        this.dataName.push("64");
        //And get data concerning "departement des retombées" and push to this.data for each departement
        break;

      //Tonalité globale
      case (this.arrayGraphics[6]):
        this.dataName.push("Tonalité positive");
        this.dataName.push("Tonalité negative");
        //And get data concerning "tonalité" and push to this.data[0]/[1]
        break;

      //Retombées par theme
      case (this.arrayGraphics[7]):
        //In the future get possible "Themes" from the server and push them
        this.dataName.push("Réseau");
        this.dataName.push("Linky/Smartgrids");
        //this.dataName.push("test"); Error message test ^^ (since data.length is, for now, fixed at 2, dataName has to be fixed at 2 otherwise index out of bounds while building dataColumns)
        // ....
        //And get data from the server concerning themes and push them to this.data
        break;

    }



      //////////////////// Data has been loaded, let's generate the graphic \\\\\\\\\\\\\\\\\\\\

    //If some data couldn't be loaded, errorOccurred = true => display an error
    if (this.data.length != this.dataName.length) {
      this.errorOcc.emit();
      return;
    }


    //Data that will be used to generate the graphic
    let dataColumns = Array(this.dataName.length);   // [ [dataName[0], data[0]...], [dataName[1], data[1]...], ... ]


      //////////////////// Build dataColumns \\\\\\\\\\\\\\\\\\\\

    for (let i = 0; i < dataColumns.length; i++) {
      let d = Array(this.data.length + 1);
      d[0] = this.dataName[i];
      for (let j = 0; j < this.data[i].length; j++) {
        d[j+1] = this.data[i][j];
      }
      dataColumns[i] = d;
    }


      //////////////////// Then, generate graphic \\\\\\\\\\\\\\\\\\\\

    let chart = c3.generate({
    bindto: '#chart',
        data: {
            columns: dataColumns,
            type: this.gType
        }
    });
  }

}


// let chart = c3.generate({
//   bindto: '#chart',
//       data: {
//           columns: [
//               ['data1', 30, 200, 100, 400, 150, 250],
//               ['data2', 50, 20, 10, 40, 15, 25]
//           ],
//           type: this.gType
//       }
//   });
