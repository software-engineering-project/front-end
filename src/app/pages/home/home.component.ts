import { Component} from '@angular/core';


export interface Tile {
  color: string;
  border: string
  cols: number;
  rows: number;
  text: string;
  textColor: string;

}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})


/**
 * Home page: Toolbar and dashboard
 */
export class HomeComponent{




  /**
   * Set background color on mouse hover. hover = color and color = hover. Is called when mouse enter to change color and mouse leave to reset color
   *
   * @param tile Tile concerned by hover
   */
  public setColorHover(tile: Tile){
    let originalColor = tile.color;
    tile.color = tile.textColor;
    tile.textColor = originalColor;
  }

  // Summary of Datas

  tilesTonaliteMed : Tile[]  = [
    {text :'Tonalité des médias' , cols : 2 , rows : 1 , textColor : "white" , color: 'rgb(0,94,184)', border: "3px solid rgb(0,94,184)"},
    {text :'Positif ' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(0,94,184)', border: "3px solid rgb(0,94,184)"},
    {text :'Négatif' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(0,94,184)', border: "3px solid rgb(0,94,184)"},
    {text :'Total' , cols : 2 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'71' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'6' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'Landes (40)' , cols : 2 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'17' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'1' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'Hautes Pyrénées (65)' , cols : 2 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'43' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'3' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'Pyrénées Atlantiques (64)' , cols : 2 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'11' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'2' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
  ];

  tilesThemeRec : Tile[] = [
    {text :'Thèmes les plus récurents' , cols : 5 , rows : 1 , textColor : '', color: "" , border: "none"},
    {text :'Thème' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(0,94,184)', border: "3px solid rgb(0,94,184)"},
    {text :'Total  ' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(0,94,184)', border: "3px solid rgb(0,94,184)"},
    {text :'Landes (40)' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(0,94,184)', border: "3px solid rgb(0,94,184)"},
    {text :'Hautes Pyrénées (65)' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(0,94,184)', border: "3px solid rgb(0,94,184)"},
    {text :'Pyrénées Atlantiques (64)' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(0,94,184)', border: "3px solid rgb(0,94,184)"},
    {text :'Partenariat' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'8' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'6' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'1' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'1' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'Sécurité' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'14' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'4' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'7' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'3' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'Transition énergétique' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'2' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'1' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'0' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'1' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'RH' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'9' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'5' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'1' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'3' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'Crise' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'11' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'2' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'1' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'8' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'Réseau' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'6' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'1' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'2' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'3' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'Linky∕Smartgrid' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'7' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'2' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'2' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'3' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'Envrionnement' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'6' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'3' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'1' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'2' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
  ]

  tilesSupportDepartement : Tile[]  = [
    {text :'Support par département' , cols : 4 , rows : 1 , textColor : '', color: "" , border: "none"},
    {text :'Support' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(0,94,184)', border: "3px solid rgb(0,94,184)"},
    {text :'Landes (40)' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(0,94,184)', border: "3px solid rgb(0,94,184)"},
    {text :'Hautes Pyrénées (65)' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(0,94,184)', border: "3px solid rgb(0,94,184)"},
    {text :'Pyrénées Atlantiques (64)' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(0,94,184)', border: "3px solid rgb(0,94,184)"},
    {text :'PQR' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'2' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'1' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'1' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'WEB' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'7' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'11' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'5' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'Radio' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'4' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'0' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'2' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'TV' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'1' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'0' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'1' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
  ]

  tilesNombreCP : Tile[] =[
    {text :'Nombre CP' , cols : 3 , rows : 1 , textColor : '', color: "" , border: "none"},
    {text :'Landes (40)' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(0,94,184)', border: "3px solid rgb(0,94,184)"},
    {text :'Hautes Pyrénées (65)' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(0,94,184)', border: "3px solid rgb(0,94,184)"},
    {text :'Pyrénées Atlantiques (64)' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(0,94,184)', border: "3px solid rgb(0,94,184)"},
    {text :'5' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'3' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'3' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
  ]

  tilesRetombeCP : Tile[] =[
    {text :'Retombées liés aux CP' , cols : 4 , rows : 1 , textColor : '', color: "" , border: "none"},
    {text :'Totales' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(0,94,184)', border: "3px solid rgb(0,94,184)"},
    {text :'Landes (40)' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(0,94,184)', border: "3px solid rgb(0,94,184)"},
    {text :'Hautes Pyrénées (65)' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(0,94,184)', border: "3px solid rgb(0,94,184)"},
    {text :'Pyrénées Atlantiques (64)' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(0,94,184)', border: "3px solid rgb(0,94,184)"},
    {text :'34' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'10' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'15' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'9' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
  ]

  tilesThemeTonalite : Tile[] = [
    {text :'Tonalité par thématique' , cols : 9 , rows : 1 , textColor : '', color: "" , border: "none"},
    {text :'Thème' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(0,94,184)', border: "3px solid rgb(0,94,184)"},
    {text :'Total positif ' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(0,94,184)', border: "3px solid rgb(0,94,184)"},
    {text :'Total négatif ' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(0,94,184)', border: "3px solid rgb(0,94,184)"},
    {text :'40 positif' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(0,94,184)', border: "3px solid rgb(0,94,184)"},
    {text :'40 négatif' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(0,94,184)', border: "3px solid rgb(0,94,184)"},
    {text :'65 positif' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(0,94,184)', border: "3px solid rgb(0,94,184)"},
    {text :'65 négatif' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(0,94,184)', border: "3px solid rgb(0,94,184)"},
    {text :'64 positif' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(0,94,184)', border: "3px solid rgb(0,94,184)"},
    {text :'64 négatif' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(0,94,184)', border: "3px solid rgb(0,94,184)"},
    {text :'Partenariat' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'11' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'0' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(239,66,111)', border: "3px solid rgb(239,66,111)"},
    {text :'5' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'0' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(239,66,111)', border: "3px solid rgb(239,66,111)"},
    {text :'6' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'0' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(239,66,111)', border: "3px solid rgb(239,66,111)"},
    {text :'0' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'0' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(239,66,111)', border: "3px solid rgb(239,66,111)"},
    {text :'Sécurité' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'0' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'0' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(239,66,111)', border: "3px solid rgb(239,66,111)"},
    {text :'0' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'0' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(239,66,111)', border: "3px solid rgb(239,66,111)"},
    {text :'0' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'0' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(239,66,111)', border: "3px solid rgb(239,66,111)"},
    {text :'0' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'0' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(239,66,111)', border: "3px solid rgb(239,66,111)"},
    {text :'Transition énergétique' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'9' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'0' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(239,66,111)', border: "3px solid rgb(239,66,111)"},
    {text :'2' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'0' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(239,66,111)', border: "3px solid rgb(239,66,111)"},
    {text :'0' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'0' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(239,66,111)', border: "3px solid rgb(239,66,111)"},
    {text :'7' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'0' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(239,66,111)', border: "3px solid rgb(239,66,111)"},
    {text :'RH' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'7' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'0' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(239,66,111)', border: "3px solid rgb(239,66,111)"},
    {text :'1' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'0' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(239,66,111)', border: "3px solid rgb(239,66,111)"},
    {text :'5' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'0' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(239,66,111)', border: "3px solid rgb(239,66,111)"},
    {text :'1' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'0' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(239,66,111)', border: "3px solid rgb(239,66,111)"},
    {text :'Crise' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'8' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'0' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(239,66,111)', border: "3px solid rgb(239,66,111)"},
    {text :'4' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'0' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(239,66,111)', border: "3px solid rgb(239,66,111)"},
    {text :'2' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'0' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(239,66,111)', border: "3px solid rgb(239,66,111)"},
    {text :'2' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'0' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(239,66,111)', border: "3px solid rgb(239,66,111)"},
    {text :'Réseau' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'25' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'1' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(239,66,111)', border: "3px solid rgb(239,66,111)"},
    {text :'3' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'0' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(239,66,111)', border: "3px solid rgb(239,66,111)"},
    {text :'16' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'0' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(239,66,111)', border: "3px solid rgb(239,66,111)"},
    {text :'6' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'1' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(239,66,111)', border: "3px solid rgb(239,66,111)"},
    {text :'Linky' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'6' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'5' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(239,66,111)', border: "3px solid rgb(239,66,111)"},
    {text :'2' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'1' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(239,66,111)', border: "3px solid rgb(239,66,111)"},
    {text :'1' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'3' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(239,66,111)', border: "3px solid rgb(239,66,111)"},
    {text :'3' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'1' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(239,66,111)', border: "3px solid rgb(239,66,111)"},
    {text :'Envrionnement' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'0' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'0' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(239,66,111)', border: "3px solid rgb(239,66,111)"},
    {text :'0' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'0' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(239,66,111)', border: "3px solid rgb(239,66,111)"},
    {text :'0' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'0' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(239,66,111)', border: "3px solid rgb(239,66,111)"},
    {text :'0' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(147,201,14)', border: "3px solid rgb(147,201,14)"},
    {text :'0' , cols : 1 , rows : 1 , textColor : "white" , color: 'rgb(239,66,111)', border: "3px solid rgb(239,66,111)"},
  ]
}
