import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})

/**
 * Create a toolbar for the whole
 */
export class ToolbarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
