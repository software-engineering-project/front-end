export const environment = {
  production: true,
  API_URL: 'https://localhost',
  API_PORT: '3000'
};
